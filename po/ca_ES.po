# VDR plugin language source file.
# Copyright (C) 2007 Klaus Schmidinger <kls@cadsoft.de>
# This file is distributed under the same license as the VDR package.
#
msgid ""
msgstr ""
"Project-Id-Version: VDR 1.5.7\n"
"Report-Msgid-Bugs-To: <softdevice-devel at lists.berlios.de>\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: \n"
"Last-Translator: \n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=ISO-8859-1\n"
"Content-Transfer-Encoding: 8bit\n"

msgid "16:9 wide"
msgstr ""

msgid "4:3 normal"
msgstr ""

msgid "none"
msgstr ""

msgid "fast"
msgstr ""

msgid "default"
msgstr ""

msgid "safe"
msgstr ""

msgid "good seeking"
msgstr ""

msgid "HDTV"
msgstr ""

msgid "playing"
msgstr ""

msgid "suspended"
msgstr ""

msgid "pseudo"
msgstr ""

msgid "software"
msgstr ""

msgid "Brightness"
msgstr ""

msgid "Contrast"
msgstr ""

msgid "Hue"
msgstr ""

msgid "Saturation"
msgstr ""

msgid "HW-Deinterlace"
msgstr ""

msgid "CropMode"
msgstr ""

msgid "CropModeToggleKey"
msgstr ""

msgid "Autodetect Movie Aspect"
msgstr ""

msgid "no"
msgstr "no"

msgid "yes"
msgstr "s�"

msgid "Zoom factor"
msgstr ""

msgid "Zoom area shift (left/right)"
msgstr ""

msgid "Zoom area shift (up/down)"
msgstr ""

msgid "Crop lines from top"
msgstr ""

msgid "Crop lines from bottom"
msgstr ""

msgid "Crop columns from left"
msgstr ""

msgid "Crop columns from right"
msgstr ""

msgid "Expand top/bottom lines"
msgstr ""

msgid "Expand left/right columns"
msgstr ""

msgid "Deinterlace Method"
msgstr ""

msgid "Postprocessing Method"
msgstr ""

msgid "Postprocessing Quality"
msgstr ""

msgid "Picture mirroring"
msgstr ""

msgid "off"
msgstr ""

msgid "on"
msgstr ""

msgid "Cropping"
msgstr ""

msgid "Post processing"
msgstr ""

msgid "Video out"
msgstr ""

msgid "Logging"
msgstr ""

msgid "Xv startup aspect"
msgstr ""

msgid "Screen Aspect"
msgstr ""

msgid "OSD alpha blending"
msgstr ""

msgid "Buffer Mode"
msgstr ""

msgid "Playback"
msgstr ""

msgid "A/V Delay"
msgstr ""

msgid "AC3 Mode"
msgstr ""

msgid "Sync Mode"
msgstr ""

msgid "Pixel Format"
msgstr ""

msgid "Use StretchBlit"
msgstr ""

msgid "Use SourceRectangle"
msgstr ""

msgid "Hide main menu entry"
msgstr ""

msgid "Info Messages"
msgstr ""

msgid "Debug Messages"
msgstr ""

msgid "Trace Messages"
msgstr ""

msgid "Logfile"
msgstr ""

msgid "Append PID"
msgstr ""

msgid "A software emulated MPEG2 device"
msgstr ""

msgid "Softdevice"
msgstr ""
