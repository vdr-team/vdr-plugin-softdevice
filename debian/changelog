vdr-plugin-softdevice (0.5.0-8) experimental; urgency=low

  * Switched to GIT using pristine tar

 -- Tobias Grimm <etobi@debian.org>  Sat, 29 Oct 2011 19:51:17 +0200

vdr-plugin-softdevice (0.5.0-7) experimental; urgency=low

  * Removed non-standard shebang line from debian/rules
  * Added README.source
  * Standards-Version: 3.8.3
  * Removed path from maintainer scripts

 -- Tobias Grimm <etobi@debian.org>  Mon, 09 Nov 2009 19:36:30 +0100

vdr-plugin-softdevice (0.5.0-6) experimental; urgency=low

  * Updated 04_debian_ffmpeg.dpatch
  * Dropped build dependency to libdfb++-dev - this package has been orphaned
    and removed from Debian, so DFB++ is not supported anymore right now

 -- Tobias Grimm <etobi@debian.org>  Sun, 03 May 2009 22:01:12 +0200

vdr-plugin-softdevice (0.5.0-5) experimental; urgency=low

  * Release for vdrdevel 1.7.6
  * Added ${misc:Depends}
  * Bumped standards version to 3.8.1
  * Updated debian/copyright
  * Changed section to "video"

 -- Thomas Günther <tom@toms-cafe.de>  Wed, 29 Apr 2009 23:11:06 +0200

vdr-plugin-softdevice (0.5.0-4) experimental; urgency=low

  * Added 07_ffmpeg_52.23.1.dpatch

 -- Tobias Grimm <etobi@debian.org>  Sun, 28 Dec 2008 13:08:34 +0100

vdr-plugin-softdevice (0.5.0-3) experimental; urgency=low

  * Build-depend on libswscale-dev because newer ffmpeg packages don't
    support some old API functions anymore, that were replaced by
    libswscale

 -- Tobias Grimm <tg@e-tobi.net>  Wed, 30 Jul 2008 20:43:29 +0200

vdr-plugin-softdevice (0.5.0-2) experimental; urgency=low

  * Dropped patchlevel control field
  * Build-Depend on vdr-dev (>=1.6.0-5)
  * Bumped Standards-Version to 3.8.0

 -- Tobias Grimm <tg@e-tobi.net>  Fri, 25 Jul 2008 19:27:28 +0200

vdr-plugin-softdevice (0.5.0-1) experimental; urgency=low

  * New upstream release
  * Added Homepage field to debian/control
  * Updated debian/copyright
  * Removed --with-ffmpeg-path, upstream now uses pkg-config

 -- Tobias Grimm <tg@e-tobi.net>  Thu, 01 May 2008 22:44:02 +0200

vdr-plugin-softdevice (0.4.0+cvs20070830-8) experimental; urgency=low

  * Build-depend on vdr-dev (>= 1.6.0)
  * Switched Build-System to cdbs, Build-Depend on cdbs
  * Renamed XS-Vcs-* fields to Vcs-* in debian/control
  * Bumped Standards-Version to 3.7.3
  * Using COMPAT=5 now

 -- Tobias Grimm <tg@e-tobi.net>  Fri, 04 Apr 2008 13:50:07 +0200

vdr-plugin-softdevice (0.4.0+cvs20070830-7) experimental; urgency=low

  * Force rebuild for vdr 1.5.15

 -- Tobias Grimm <tg@e-tobi.net>  Mon, 18 Feb 2008 21:08:59 +0100

vdr-plugin-softdevice (0.4.0+cvs20070830-6) unstable; urgency=low

  * Release for vdrdevel 1.5.13

 -- Thomas Günther <tom@toms-cafe.de>  Wed, 16 Jan 2008 10:31:38 +0100

vdr-plugin-softdevice (0.4.0+cvs20070830-5) unstable; urgency=low

  * Release for vdrdevel 1.5.12

 -- Thomas Günther <tom@toms-cafe.de>  Tue, 20 Nov 2007 23:46:30 +0100

vdr-plugin-softdevice (0.4.0+cvs20070830-4) unstable; urgency=low

  * Release for vdrdevel 1.5.11

 -- Thomas Günther <tom@toms-cafe.de>  Tue,  6 Nov 2007 23:34:21 +0100

vdr-plugin-softdevice (0.4.0+cvs20070830-3) unstable; urgency=low

  * Release for vdrdevel 1.5.10

 -- Thomas Günther <tom@toms-cafe.de>  Tue, 16 Oct 2007 23:51:02 +0200

vdr-plugin-softdevice (0.4.0+cvs20070830-2) unstable; urgency=low

  [ Thomas Schmidt ]
  * Add user vdr to the group audio with the vdr-groups-script
  * Build-Depend on vdr-dev (>=1.4.7-1) for vdr-groups.sh

  [ Tobias Grimm ]
  * Added build dependency to pkg-config
  * Improved debian/copyright
  * Improved package description

  [ Thomas Günther ]
  * Switched to new upstream changelog file CHANGELOG
  * Fixed debian-rules-ignores-make-clean-error

 -- Thomas Günther <tom@toms-cafe.de>  Sun,  7 Oct 2007 22:09:24 +0200

vdr-plugin-softdevice (0.4.0+cvs20070830-1) unstable; urgency=low

  * New upstream cvs release

 -- Thomas Günther <tom@toms-cafe.de>  Tue,  4 Sep 2007 23:20:43 +0200

vdr-plugin-softdevice (0.4.0-5) unstable; urgency=low

  * Release for vdrdevel 1.5.8

 -- Thomas Günther <tom@toms-cafe.de>  Thu, 23 Aug 2007 01:09:10 +0200

vdr-plugin-softdevice (0.4.0-4) unstable; urgency=low

  * Release for vdrdevel 1.5.6

 -- Thomas Günther <tom@toms-cafe.de>  Tue, 14 Aug 2007 01:46:22 +0200

vdr-plugin-softdevice (0.4.0-3) unstable; urgency=low

  [ Thomas Schmidt ]
  * Added XS-Vcs-Svn and XS-Vcs-Browser fields to debian/control

  [ Thomas Günther ]
  * Release for vdrdevel 1.5.5

 -- Thomas Günther <tom@toms-cafe.de>  Wed, 27 Jun 2007 23:02:45 +0200

vdr-plugin-softdevice (0.4.0-2) unstable; urgency=low

  * Release for vdrdevel 1.5.2

 -- Thomas Günther <tom@toms-cafe.de>  Sat, 28 Apr 2007 01:20:44 +0200

vdr-plugin-softdevice (0.4.0-1) unstable; urgency=low

  * New upstream release
  * Dropped 91_softdevice-0.3.1-1.5.0.dpatch - fixed upstream
  * Build-Depend on libxinerama-dev to enable Xinerama support (This will not
    compile on Sarge anymore!)

 -- Tobias Grimm <tg@e-tobi.net>  Thu, 12 Apr 2007 22:42:36 +0200

vdr-plugin-softdevice (0.3.1-4) experimental; urgency=low

  * Release for vdrdevel 1.5.1

 -- Thomas Günther <tom@toms-cafe.de>  Tue, 27 Feb 2007 19:59:21 +0100

vdr-plugin-softdevice (0.3.1-3) experimental; urgency=low

  * Added 91_softdevice-0.3.1-1.5.0.dpatch
  * Added myself to Uploaders

 -- Thomas Günther <tom@toms-cafe.de>  Sun, 21 Jan 2007 16:26:21 +0100

vdr-plugin-softdevice (0.3.1-2) experimental; urgency=low

  [ Thomas Günther ]
  * Replaced VDRdevel adaptions in debian/rules with make-special-vdr
  * Adapted call of dependencies.sh and patchlevel.sh to the new location
    in /usr/share/vdr-dev/

  [ Tobias Grimm ]
  * Build-Depend on vdr-dev (>=1.4.5-1)

 -- Tobias Grimm <tg@e-tobi.net>  Sun, 14 Jan 2007 20:16:08 +0100

vdr-plugin-softdevice (0.3.1-1) unstable; urgency=low

  * New upstream release

 -- Tobias Grimm <tg@e-tobi.net>  Mon,  6 Nov 2006 21:45:47 +0100

vdr-plugin-softdevice (0.3.0-4) unstable; urgency=low

  * Build-Depend on vdr-dev (>=1.4.4-1)

 -- Tobias Grimm <tg@e-tobi.net>  Sun,  5 Nov 2006 00:00:45 +0100

vdr-plugin-softdevice (0.3.0-3) unstable; urgency=low

  * Removed 07_amd_64.dpatch - fixed upstream

 -- Tobias Grimm <tg@e-tobi.net>  Thu,  2 Nov 2006 18:38:52 +0100

vdr-plugin-softdevice (0.3.0-2) unstable; urgency=low

  * Removed --disable-mmx2 and --disable-mmx from configure call

 -- Tobias Grimm <tg@e-tobi.net>  Mon, 16 Oct 2006 20:51:11 +0200

vdr-plugin-softdevice (0.3.0-1) unstable; urgency=low

  * New upstream release
  * Removed 02_disable_unsupported_output.dpatch
  * Removed 03_no_sub_libs.dpatch
  * Rebuilt build dependencies by a clever script that finds the packages which
    provide the include files referenced in the sources - I hope
    the results are correct
  * Added 07_amd_64.dpatch to fix AMD64 compilation problem as reported
    by Ingo Theiss <cthulhu@planet-multiplayer.de>

 -- Tobias Grimm <tg@e-tobi.net>  Sun, 15 Oct 2006 13:40:38 +0200

vdr-plugin-softdevice (0.2.3a-7) unstable; urgency=low

  * Added 05_FFMPEG_VERSION.dpatch

 -- Tobias Grimm <tg@e-tobi.net>  Sun,  1 Oct 2006 17:14:18 +0200

vdr-plugin-softdevice (0.2.3a-6) unstable; urgency=low

  [ Thomas Schmidt ]
  * Build-Depend on vdr-dev (>=1.4.3-1)

 -- Tobias Grimm <tg@e-tobi.net>  Sat, 30 Sep 2006 16:37:16 +0200

vdr-plugin-softdevice (0.2.3a-5) unstable; urgency=low

  * Bumped Standards-Version to 3.7.2
  * Added note about the Debian Maintainers to debian/copyright
  * Added Tobias Grimm to Uploaders
  * Build-Depend on vdr-dev (>=1.4.2-1), libxv-dev and libxi-dev

 -- Thomas Schmidt <tschmidt@debian.org>  Mon, 28 Aug 2006 21:00:05 +0200

vdr-plugin-softdevice (0.2.3a-4) unstable; urgency=low

  * Force rebuild for libdfb++-0.9-25 in Sid
  * Added libdirectfb-dev to build dependencies

 -- Tobias Grimm <tg@e-tobi.net>  Wed, 23 Aug 2006 21:15:04 +0200

vdr-plugin-softdevice (0.2.3a-3) unstable; urgency=low

  * Thomas Schmidt <tschmidt@debian.org>
    - Build-Depend on vdr-dev (>=1.4.1-1)

 -- Debian VDR Team <pkg-vdr-dvb-devel@lists.alioth.debian.org>  Sun, 18 Jun 2006 15:55:41 +0200

vdr-plugin-softdevice (0.2.3a-2) unstable; urgency=low

  * Tobias Grimm <tg@e-tobi.net>
    - Increased build dependecy of libavcodec-dev to >= 0.cvs20060329-4
    - Modified 02_ffmpeg.dpatch to not use --plugin-libs anymore, because
      libavcodec is now linked dynamically

 -- Debian VDR Team <pkg-vdr-dvb-devel@lists.alioth.debian.org>  Thu, 18 May 2006 19:44:40 +0200

vdr-plugin-softdevice (0.2.3a-1) unstable; urgency=low

  * New upstream release
  * Thomas Schmidt <tschmidt@debian.org>
    - Build-Depend on vdr-dev (>=1.4.0-1)
  * Tobias Grimm <tg@e-tobi.net>
    - Updated watch file
    - Removed 90_APIVERSION.dpatch
    - Added installation of ShmClient

 -- Debian VDR Team <pkg-vdr-dvb-devel@lists.alioth.debian.org>  Wed,  3 May 2006 22:18:51 +0200

vdr-plugin-softdevice (0.2.2-4) unstable; urgency=low

  * Thomas Schmidt <tschmidt@debian.org>
    - Build-Depend on vdr-dev (>=1.3.48-1)
  * Tobias Grimm <tg@e-tobi.net>
    - Added 90_APIVERSION.dpatch

 -- Debian VDR Team <pkg-vdr-dvb-devel@lists.alioth.debian.org>  Mon, 24 Apr 2006 20:17:49 +0200

vdr-plugin-softdevice (0.2.2-3) unstable; urgency=low

  * Tobias Grimm <tg@e-tobi.net>
    - Added watch file - downloding doesn't work, but checking for new versions
  * Thomas Schmidt <tschmidt@debian.org>
    - Build-Depend on vdr-dev (>=1.3.46-1)

 -- Debian VDR Team <pkg-vdr-dvb-devel@lists.alioth.debian.org>  Thu, 13 Apr 2006 00:54:41 +0200

vdr-plugin-softdevice (0.2.2-2) unstable; urgency=low

  * Thomas Schmidt <tschmidt@debian.org>
    - Build-Depend on vdr-dev (>=1.3.45-1)

 -- Debian VDR Team <pkg-vdr-dvb-devel@lists.alioth.debian.org>  Mon, 27 Mar 2006 23:18:57 +0200

vdr-plugin-softdevice (0.2.2-1) unstable; urgency=low

  * Tobias Grimm <tg@e-tobi.net>
    - New upstream release
    - Removed 01_Makefile-fPIC-fix - fixed in upstream
    - Removed watch file, doesn't work with BerliOS anymore

 -- Debian VDR Team <pkg-vdr-dvb-devel@lists.alioth.debian.org>  Thu, 23 Mar 2006 21:30:51 +0100

vdr-plugin-softdevice (0.2.1-4) unstable; urgency=low

  * Tobias Grimm <tg@e-tobi.net>
    - Depend/Build-Depend on vdr (>=1.3.41-1)

 -- Debian VDR Team <pkg-vdr-dvb-devel@lists.alioth.debian.org>  Tue, 31 Jan 2006 20:03:25 +0100

vdr-plugin-softdevice (0.2.1-3) unstable; urgency=low

  * Tobias Grimm <tg@e-tobi.net>
    - Build-Depend on vdr-dev (>=1.3.40-1)

 -- Debian VDR Team <pkg-vdr-dvb-devel@lists.alioth.debian.org>  Thu, 26 Jan 2006 19:36:45 +0100

vdr-plugin-softdevice (0.2.1-2) unstable; urgency=low

  * Thomas Schmidt <tschmidt@debian.org>
    - Build-Depend on vdr (>=1.3.38-1)

 -- Debian VDR Team <pkg-vdr-dvb-devel@lists.alioth.debian.org>  Thu, 12 Jan 2006 16:57:05 +0100

vdr-plugin-softdevice (0.2.1-1) unstable; urgency=low

  * Tobias Grimm <tg@e-tobi.net>
    - New upstream release  
    - Updated 01_Makefile-fPIC-fix.dpatch
    - Updated 04_debian_ffmpeg.dpatch
    - Updated 06_dfb++.dpatch
  * Thomas Schmidt <tschmidt@debian.org>
    - Build-Depend on vdr (>=1.3.37-1)

 -- Debian VDR Team <pkg-vdr-dvb-devel@lists.alioth.debian.org>  Tue, 29 Nov 2005 15:25:22 +0100

vdr-plugin-softdevice (0.1.3-3) unstable; urgency=low

  * Thomas Schmidt <tschmidt@debian.org>
    - Build-Depend on vdr (>=1.3.33-1)

 -- Debian VDR Team <pkg-vdr-dvb-devel@lists.alioth.debian.org>  Sun,  2 Oct 2005 12:21:48 +0200

vdr-plugin-softdevice (0.1.3-2) unstable; urgency=low
  
  * Thomas Schmidt <tschmidt@debian.org>
    - Build-Depend on vdr (>=1.3.32-1)

 -- Debian VDR Team <pkg-vdr-dvb-devel@lists.alioth.debian.org>  Sun, 11 Sep 2005 21:34:48 +0200

vdr-plugin-softdevice (0.1.3-1) unstable; urgency=low

  * Tobias Grimm <tg@e-tobi.net>
    - New upstream release
    - Updated 01_Makefile-fPIC-fix, 04_debian_ffmpeg, 06_dfb++.dpatch
    - Removed 05_offset_t (fixed in upstream)

 -- Tobias Grimm <tg@e-tobi.net>  Sun,  4 Sep 2005 16:11:19 +0200

vdr-plugin-softdevice (0.1.2-8) unstable; urgency=low

  * Thomas Schmidt <tschmidt@debian.org>
    - Build-Depend on vdr (>=1.3.31-1)
    - Use dependencies.sh from vdr-dev to set vdr dependencies and
      conflicts to the right vdr version when building the package

 -- Debian VDR Team <pkg-vdr-dvb-devel@lists.alioth.debian.org>  Sun, 28 Aug 2005 19:36:08 +0200

vdr-plugin-softdevice (0.1.2-7) unstable; urgency=low

  * Tobias Grimm <tg@e-tobi.net>
    - Depend/Build-Depend on vdr (>=1.3.30-1)
    - Conflict with vdr (>=1.3.30.99)

 -- Debian VDR Team <pkg-vdr-dvb-devel@lists.alioth.debian.org>  Tue, 23 Aug 2005 19:18:44 +0200

vdr-plugin-softdevice (0.1.2-6) unstable; urgency=low
 
  * Thomas Schmidt <tschmidt@debian.org>
    - Depend/Build-Depend on vdr (>=1.3.29-1)
    - Conflict with vdr (>=1.3.29.99)

 -- Debian VDR Team <pkg-vdr-dvb-devel@lists.alioth.debian.org>  Mon, 15 Aug 2005 19:37:00 +0200

vdr-plugin-softdevice (0.1.2-5) unstable; urgency=low
  
  * Thomas Schmidt <tschmidt@debian.org>
    - Depend/Build-Depend on vdr (>=1.3.28-1)
    - Conflict with vdr (>=1.3.29)

 -- Debian VDR Team <pkg-vdr-dvb-devel@lists.alioth.debian.org>  Wed, 10 Aug 2005 20:57:04 +0200

vdr-plugin-softdevice (0.1.2-4) unstable; urgency=low

  * Thomas Schmidt <tschmidt@debian.org>
    - Depend/Build-Depend on vdr (>=1.3.27-1)
    - Conflict with vdr (>=1.3.28)
    - Converted debian/changelog to UTF-8, to make lintian happy
  * Tobias Grimm <tg@e-tobi.net>
    - Bumped Standards-Version to 3.6.2
    - Using dfb++ now
    - Now again only the official debian ffmpeg package is used

 -- Debian VDR Team <pkg-vdr-dvb-devel@lists.alioth.debian.org>  Sun, 19 Jun 2005 17:58:06 +0200

vdr-plugin-softdevice (0.1.2-3) unstable; urgency=low

  * Thomas Schmidt <tschmidt@debian.org>
    - Depend/Build-Depend on vdr (>=1.3.26-1)
    - Conflict with vdr (>=1.3.27)

 -- Debian VDR Team <pkg-vdr-dvb-devel@lists.alioth.debian.org>  Sun, 12 Jun 2005 17:10:17 +0200

vdr-plugin-softdevice (0.1.2-2) experimental; urgency=low

  * Thomas Schmidt <tschmidt@debian.org> 
    - Depend/Build-Depend on vdr (>=1.3.25-1)
    - Conflict with vdr (>=1.3.26)

 -- Debian VDR Team <pkg-vdr-dvb-devel@lists.alioth.debian.org>  Tue, 31 May 2005 16:50:33 +0200

vdr-plugin-softdevice (0.1.2-1) experimental; urgency=low

  * Tobias Grimm <tg@e-tobi.net>
    - New upstream release
    - Disabled libpostproc usage
    - Use shared ffmpeg libs from Christian Marillat, if available

 -- Debian VDR Team <pkg-vdr-dvb-devel@lists.alioth.debian.org>  Sun, 22 May 2005 15:48:54 +0200

vdr-plugin-softdevice (0.1.1-8) unstable; urgency=low

  * Tobias Grimm <tg@e-tobi.net>
    - Fixed debian/copyright
    - Use debian/compat, set debhelper compatibility to 4
    - Use dh_install 
    - Don't install empty plugin.softdevice.conf anymore
    - Using standard debian/rules now
    - Added debian/watch file for uscan
    - Removed debian/dirs
    - Replaced old patchlevel tag with XB-VDR-Patchlevel
    - Set Standards-Version to 3.6.1
    - Set Architecture to any
    - Depend on debhelper (>> 4.1.16)
    - Removed unnecessary non-source dependencies
    - Added libpostproc-dev, libavcodec-dev and libavformat-dev dependencies
    - Removed old debian/patches directory
    - Extracted all changes to the Makefile into dpatch patches
    - Added 01_Makefile-fPIC-fix.dpatch
    - Changed maintainer to Debian VDR Team 
    - Depend/Build-Depend on vdr (>=1.3.24-1)
    - Conflict with vdr (>=1.3.25)

 -- Debian VDR Team <pkg-vdr-dvb-devel@lists.alioth.debian.org>  Thu, 19 May 2005 20:07:25 +0200

vdr-plugin-softdevice (0.1.1-nh.7) unstable; urgency=low

  * New VDR version (NH)

 -- Nicolas Huillard <nicolas@huillard.net>  Thu, 28 Apr 2005 17:09:01 +0200

vdr-plugin-softdevice (0.1.1-nh.6) unstable; urgency=low

  * Using DirectFB 0.9.22-20050424 (NH)
  * commented-out all code in video-dfb.c : static void reportCardInfo
    (IDirectFB *dfb) because DFBCardCapabilities does not exist anymore in
    DirectFB (Subject: Re: [Softdevice-devel] latest softdevice CVS & latest
    DirectFB & DFB++ cvs broken??  From: Torgeir Veimo <torgeir@pobox.com> To:
    softdevice-devel@berlios.de Date: Tue, 12 Apr 2005 00:58:15 +0100)

 -- Nicolas Huillard <nicolas@huillard.net>  Sun, 24 Apr 2005 23:57:15 +0200

vdr-plugin-softdevice (0.1.1-nh.5) unstable; urgency=low

  * New upstream version, with VDR 1.3.23 Darren Salt
  * Depends on libavcodeccvs / libavcodeccvs-dev (NH)
  * Output methods build as a single lib, for runvdr to run them correctly
  * Also : Apr 24 18:56:20 vdr vdr[6787]: [softdevice] could not load
    (./PLUGINS/lib/libvdr-softdevice-fb.so.1.3.23)[./PLUGINS/lib/libvdr-softdevice-fb.so.1.3.23:
    cannot open shared object file: No such file or directory] exiting
  * Recompile with out "not as root" patch for DirectFB to run (NH)

 -- Nicolas Huillard <nicolas@huillard.net>  Sun, 24 Apr 2005 19:54:04 +0200

vdr-plugin-softdevice (0.1.1-nh.3) unstable; urgency=low

  * New upstream version, with VDR 1.3.23 Darren Salt
  * Depends on libavcodeccvs / libavcodeccvs-dev (NH)
  * Output methods build as a single lib, for runvdr to run them correctly
  * Also : Apr 24 18:56:20 vdr vdr[6787]: [softdevice] could not load
    (./PLUGINS/lib/libvdr-softdevice-fb.so.1.3.23)[./PLUGINS/lib/libvdr-softdevice-fb.so.1.3.23:
    cannot open shared object file: No such file or directory] exiting

 -- Nicolas Huillard <nicolas@huillard.net>  Sun, 24 Apr 2005 18:57:27 +0200

vdr-plugin-softdevice (0.1.1-nh.2) unstable; urgency=low

  * New upstream version, with VDR 1.3.23 Darren Salt
  * Depends on libavcodeccvs / libavcodeccvs-dev (NH)

 -- Nicolas Huillard <nicolas@huillard.net>  Sun, 24 Apr 2005 18:39:08 +0200

vdr-plugin-softdevice (0.1.1-nh.1) unstable; urgency=low

  * New upstream version, with VDR 1.3.23 Darren Salt

 -- Nicolas Huillard <nicolas@huillard.net>  Sun, 24 Apr 2005 18:32:43 +0200

vdr-plugin-softdevice (0.0.7cvs-20040927.1) unstable; urgency=low

  * recompile with vdr + ffdecsa

 -- Nicolas Huillard <nicolas@huillard.net>  Mon, 27 Sep 2004 23:44:27 +0200

vdr-plugin-softdevice (0.0.7cvs-20040813.2) unstable; urgency=low

  * recompile with vdr + csa

 -- Nicolas Huillard <nicolas@huillard.net>  Fri, 13 Aug 2004 15:43:16 +0200

vdr-plugin-softdevice (0.0.7cvs-20040813.1) unstable; urgency=low

  * slight improve of the Makefile mods
  * activated MMX2, AKA SSE (see /proc/cpuinfo)

 -- Nicolas Huillard <nicolas@huillard.net>  Fri, 13 Aug 2004 00:04:30 +0200

vdr-plugin-softdevice (0.0.7cvs-20040810.1) unstable; urgency=low

  * fresh cvs checkout 20040810

 -- Nicolas Huillard <nicolas@huillard.net>  Tue, 10 Aug 2004 12:12:29 +0200

vdr-plugin-softdevice (0.0.7cvs-20040805.1) unstable; urgency=low

  * fresh cvs checkout 20040805
  * Makefile réadapté à ma conf...
  * pas de -DUSE_MMX2 ni de PP_LIBAVCODEC

 -- Nicolas Huillard <nicolas@huillard.net>  Thu,  5 Aug 2004 23:07:03 +0200

vdr-plugin-softdevice (0.0.7pre2-20040803.1) unstable; urgency=low

  * recompilation après installation de ffmpeg 0.4.9pre1

 -- Nicolas Huillard <nicolas@huillard.net>  Wed,  4 Aug 2004 00:50:43 +0200

vdr-plugin-softdevice (0.0.7pre2-20040802.2) unstable; urgency=low

  * patch de Stephan Lucke pour OSD alpha
  * essai avec 0x80 au lieu de 0xff

 -- Nicolas Huillard <nicolas@huillard.net>  Mon,  2 Aug 2004 23:17:00 +0200

vdr-plugin-softdevice (0.0.7pre2-20040802.1) unstable; urgency=low

  * patch de Stephan Lucke pour OSD alpha
  * Subject: [vdr] Re: Softdevice 0.0.7pre2 crash w/ DFB, CLE266
  * Date: Mon, 2 Aug 2004 07:13:57 +0200

 -- Nicolas Huillard <nicolas@huillard.net>  Mon,  2 Aug 2004 23:06:07 +0200

vdr-plugin-softdevice (0.0.7pre2-20040801.1) unstable; urgency=low

  * throw dans IDirectFBDisplayLayer::SetScreenLocation()
  * changement de 720x576 en dur par 1280x1024

 -- Nicolas Huillard <nicolas@huillard.net>  Sun,  1 Aug 2004 12:39:23 +0200

vdr-plugin-softdevice (0.0.7pre2-20040731.7) unstable; urgency=low

  * throw dans IDirectFBDisplayLayer::SetScreenLocation()
  * explication de l'exception

 -- Nicolas Huillard <nicolas@huillard.net>  Sat, 31 Jul 2004 16:42:00 +0200

vdr-plugin-softdevice (0.0.7pre2-20040731.6) unstable; urgency=low

  * throw dans IDirectFBDisplayLayer::SetScreenLocation()
  * sans MMX2

 -- Nicolas Huillard <nicolas@huillard.net>  Sat, 31 Jul 2004 16:25:51 +0200

vdr-plugin-softdevice (0.0.7pre2-20040731.5) unstable; urgency=low

  * throw dans IDirectFBDisplayLayer::SetScreenLocation()
  * autre test

 -- Nicolas Huillard <nicolas@huillard.net>  Sat, 31 Jul 2004 16:08:00 +0200

vdr-plugin-softdevice (0.0.7pre2-20040731.4) unstable; urgency=low

  * throw dans IDirectFBDisplayLayer::SetScreenLocation()
  * autre test

 -- Nicolas Huillard <nicolas@huillard.net>  Sat, 31 Jul 2004 15:49:51 +0200

vdr-plugin-softdevice (0.0.7pre2-20040731.3) unstable; urgency=low

  * throw dans IDirectFBDisplayLayer::SetScreenLocation()
  * video-dfb.c :
    497         //if (desc.caps & DLCAPS_SCREEN_LOCATION)
    498         if (0)

 -- Nicolas Huillard <nicolas@huillard.net>  Sat, 31 Jul 2004 14:52:05 +0200

vdr-plugin-softdevice (0.0.7pre2-20040731.2) unstable; urgency=low

  * Recompile after DirectFB patch
  * Epia user with DirectFB have to place "disable-module=cle266" in
    /etc/directfbrc to get a working OSD and have to apply attached patch to
    DirectFB

 -- Nicolas Huillard <nicolas@huillard.net>  Sat, 31 Jul 2004 14:12:32 +0200

vdr-plugin-softdevice (0.0.7pre2-1) unstable; urgency=low

  * New upstream version

 -- Nicolas Huillard <nicolas@huillard.net>  Sat, 31 Jul 2004 12:44:19 +0200

vdr-plugin-softdevice (0.0.6-1.nh8) unstable; urgency=low

  * dlc.options = (DFBDisplayLayerOptions)( DLOP_DST_COLORKEY |
    DLOP_DEINTERLACING );
  * dlc.pixelformat = DSPF_YUY2;
  * #define COLORKEY 0,0,0

 -- Nicolas Huillard <nicolas@huillard.net>  Wed, 30 Jun 2004 00:17:13 +0200

vdr-plugin-softdevice (0.0.6-1.nh7) unstable; urgency=low

  * dlc.options = (DFBDisplayLayerOptions)( DLOP_DST_COLORKEY |
    DLOP_DEINTERLACING );
  * dlc.pixelformat = DSPF_YV12;

 -- Nicolas Huillard <nicolas@huillard.net>  Fri, 25 Jun 2004 00:15:02 +0200

vdr-plugin-softdevice (0.0.6-1.nh6) unstable; urgency=low

  * dlc.pixelformat = DSPF_YUY2 in SetParams

 -- Nicolas Huillard <nicolas@huillard.net>  Fri, 18 Jun 2004 00:52:02 +0200

vdr-plugin-softdevice (0.0.6-1.nh5) unstable; urgency=low

  * DLOP_NONE in SetParams

 -- Nicolas Huillard <nicolas@huillard.net>  Fri, 18 Jun 2004 00:35:02 +0200

vdr-plugin-softdevice (0.0.6-1.nh4) unstable; urgency=low

  * Xv support + bugfix in DFB

 -- Nicolas Huillard <nicolas@huillard.net>  Tue,  8 Jun 2004 10:15:32 +0200

vdr-plugin-softdevice (0.0.6-1.nh3) unstable; urgency=low

  * set default mode to 720x576 instead of 768x576 (NH)

 -- Nicolas Huillard <nicolas@huillard.net>  Tue,  8 Jun 2004 01:07:57 +0200

vdr-plugin-softdevice (0.0.6-1.nh2) unstable; urgency=low

  * compiled with DirectFB 0.9.21 (CVS)

 -- Nicolas Huillard <nicolas@huillard.net>  Sun,  6 Jun 2004 19:58:27 +0200

vdr-plugin-softdevice (0.0.6-1.nh1) unstable; urgency=low

  * compiled with DirectFB

 -- Nicolas Huillard <nicolas@huillard.net>  Sat,  5 Jun 2004 14:41:24 +0200

vdr-plugin-softdevice (0.0.6-1) unstable; urgency=low

  * debianized

 -- Nicolas Huillard <nicolas@huillard.net>  Wed,  2 Jun 2004 23:44:21 +0200
